#!/usr/bin/env python

from time import sleep
from sys import exit
import datetime

try:
    from pyfiglet import figlet_format
except ImportError:
    exit("This script requires the pyfiglet module\nInstall with: sudo pip install pyfiglet")

import unicornhat as unicorn


unicorn.set_layout(unicorn.AUTO)
unicorn.rotation(0)
unicorn.brightness(0.5)
width,height=unicorn.get_shape()
i = -1

def createMatrix(txt):
	figletText = figlet_format(txt + ' ', "banner", width=1000) # banner font generates text with heigth 7
	textMatrix = figletText.split("\n")[:width] # width should be 8 on both HAT and pHAT!
	return textMatrix 


def createMatrixWithTime():
    print 'creating matrix'
    return createMatrix(datetime.datetime.now().strftime('%H:%M'))

textMatrix = createMatrixWithTime()

def step():
    global i
    global textMatrix
    if not i % 20:
	    textMatrix = createMatrixWithTime()
    else:
	sleep(0.1)

    textWidth = len(textMatrix[0]) # the total length of the result from figlet
    i = 0 if i>=1000*textWidth else i+1 # avoid overflow
    for h in range(height):
        for w in range(width):
            hPos = (i+h) % textWidth
            chr = textMatrix[w][hPos]
            if chr == ' ':
                unicorn.set_pixel(width - w - 1, h, 0, 0, 0)
            else:
                unicorn.set_pixel(width - w - 1, h, 255, 128, 0)
    unicorn.show()

while True:
    step()
